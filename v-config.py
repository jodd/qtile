# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# 
# WorkInProgress by jan 2021
# DTOS: 4 lines
# import re
# import socket
# import subprocess
# from libqtile import qtile


import os
import re
import socket
from libqtile import qtile
import subprocess
from typing import List  # noqa: F401
from libqtile import bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, KeyChord, Key, Match, Screen, DropDown, ScratchPad
from libqtile.lazy import lazy
# ------------------------------------------------------------------ #
@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~/.config/qtile/scripts/autostart.sh')
    subprocess.call([home])
# ------------------------------------------------------------------ #


alt = "mod1"
mod = "mod4"
terminal = "alacritty"
xterminal = "urxvt"
# from libqtile.utils import guess_terminal
# terminal = guess_terminal()
# sterminal =  "st"
lbin = "/home/jan/.local/bin/"

# ------------------------------------------------------------------ #
# keys
keys = [
#   +a +b c d +e f (g) h i j k l (s) n o p q r +s t u (v) (w) +x +y +z
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
# ------------------------------------------------------------------ #
    Key([mod], "c", lazy.spawn(lbin+'conky-toggle')),
    Key([mod], "d", lazy.spawn('rofi -show run')),
    Key([mod], "f", lazy.spawn(lbin+'TOGGLE picom'), lazy.window.toggle_fullscreen(), desc="Fullscreen no Picom"),
    Key([mod], "i", lazy.spawn(lbin+'IP-info'), desc="show info about LAN/WAN"),
    Key([mod], "n", lazy.layout.normalize(), lazy.layout.reset(), desc="Reset all window sizes, bsp as well as monads..."),
    Key([mod], "o", lazy.spawn(lbin+'RUN loffice 5'), lazy.group[("5")].toscreen(), desc="Libre Office Suite"),
    Key([mod], "p", lazy.spawn(lbin+'TOGGLE picom'), desc="Toggle picom"),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),
    Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),
    Key([mod], "t", lazy.window.toggle_floating(),  desc="Toggle floating active window"),
    Key([mod], "u", lazy.widget["keyboardlayout"].next_keyboard(), desc="change between no and us keyboard"),
# ------------------------------------------------------------------ #
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    Key([mod], "space", lazy.spawn(lbin+'ranger-run'), lazy.group[("9")].toscreen(toggle=True)),
    Key([mod], "Left", lazy.screen.toggle_group()),
    Key([mod], "Right", lazy.screen.next_group(skip_empty=True)),
    Key([mod], "Print", lazy.spawn(lbin+'screenshot'), desc="PRT SC screenshot"),
    Key([mod], "KP_Enter", lazy.group['scratchpad'].dropdown_toggle('speed'), desc="Calculator Kalkulator Speedcrunch"),
    Key([mod], "KP_Add", lazy.group['scratchpad'].dropdown_toggle('bitw'), desc="Passwordmanager Bitwarden"),
    Key([mod], "KP_Divide", lazy.prev_layout(), desc="Toggle layouts, previous Layout"),
    Key([mod], "KP_Multiply", lazy.next_layout(), desc="Toggle layouts, next Layout"),
    Key([mod], "Up", lazy.spawn('amixer sset Master 5%+'), desc="ALSA amixer volume up"),
    Key([mod], "Down", lazy.spawn('amixer sset Master 5%-'), desc="ALSA amixer volume down"),
    # Key([mod], "Up", lazy.layout.section_up(), desc="moves TreeTab desktop up"),
    # Key([mod], "Down", lazy.layout.section_down(), desc="moves TreeTab desktop down"),
# ------------------------------------------------------------------ #
#   +a +b +c +d +e +f +g h +i j k l +m +n +o +p +q +r +s +t +u +v +w +x +y +z
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
# ------------------------------------------------------------------ #
    Key([mod, "shift"], "Return", lazy.spawn(xterminal), desc="Launch terminal"),
    Key([mod, "shift"], "Left", lazy.screen.prev_group(skip_empty=False)),
    Key([mod, "shift"], "Right", lazy.screen.next_group(skip_empty=False)),
    Key([mod, "shift"], "Print", lazy.spawn(lbin+'screenshot-preview'), desc="PRT SC Screenshot and preview"),
    Key([mod, "shift"], "Up", lazy.spawn('amixer sset Master unmute; \
            amixer sset mixer unmute; \
            amixer sset headphone unmute'), desc="ALSA amixer unmute"),
    Key([mod, "shift"], "Down", lazy.spawn('amixer sset Master mute'), desc="ALSA amixer mute"),
# ------------------------------------------------------------------ #
#   +a +b c +d +e +f +g h +i j k l m +n +o +p q r +s +t +u +v +w +x +y +z
    Key([mod, "control"], "h", lazy.layout.grow_left(), lazy.layout.shrink(), lazy.layout.decrease_ratio(), lazy.layout.add()),
    Key([mod, "control"], "j", lazy.layout.grow_down(), lazy.layout.shrink(), lazy.layout.increase_nmaster()),
    Key([mod, "control"], "k", lazy.layout.grow_up(), lazy.layout.grow(), lazy.layout.decrease_nmaster()),
    Key([mod, "control"], "l", lazy.layout.grow_right(), lazy.layout.grow(), lazy.layout.increase_ratio(), lazy.layout.delete()),
    
    Key([mod, "control"], "m", lazy.group['scratchpad'].dropdown_toggle('mixer'), desc="ALSA Mixer"),
    Key([mod, "control"], "c", lazy.reload_config(), desc="Reload config"),
    # Key([mod, "control"], "f", lazy.window.enable_floating(), desc="Active window goes floating"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutting down qtile"),
    Key([mod, "control"], "r", lazy.restart(), desc="Restart Qtile"),
    # Key([mod, "control"], "t", lazy.window.disable_floating(), desc="Active window goes back tiling"),
    #
    # /Keys

    # KeyChord
    #  web ----------------------------------------------------------#
    KeyChord([mod], "w", [
        # Key([mod], "b", lazy.spawn(lbin+'RUN "flatpak run com.brave.Browser" 2'), lazy.group[("2")].toscreen(), desc="webbrowser"),
        Key([mod], "b", lazy.spawn(lbin+'FRUN brave'), lazy.group[("2")].toscreen(), desc="webbrowser"),
        Key([mod], "c", lazy.spawn(lbin+'FRUN chromium'), lazy.group[("2")].toscreen(), desc="webbrowser"),
        Key([mod], "f", lazy.spawn(lbin+'FRUN firefox'), lazy.group[("2")].toscreen(), desc="webbrowser"),
        Key([mod], "m", lazy.spawn(lbin+'RUN thunderbird 2'), lazy.group[("2")].toscreen(), desc="e-mail epost thunderbird"),
        Key([mod], "q", lazy.spawn(lbin+'qutebrowser-start'), lazy.group[("2")].toscreen(toggle=True), desc="webbrowser"),
        ]),
    # gfx -----------------------------------------------------------#
    KeyChord([mod], "g", [
        Key([mod], "d", lazy.spawn(lbin+'RUN darktable 6'), lazy.group[("6")].toscreen(), desc="darktable gfx"),
        Key([mod], "g", lazy.spawn(lbin+'RUN gimp 5'), lazy.group[("5")].toscreen(), desc="GIMP gfx"),
        Key([mod], "i", lazy.spawn(lbin+'RUN inkscape 6'), lazy.group[("6")].toscreen(), desc="inkscape gfx"),
        Key([mod], "p", lazy.spawn(lbin+'RUN gphoto2.sh'), lazy.group[("6")].toscreen(), desc="PhotoDownloader"),
        Key([mod], "v", lazy.spawn(lbin+'RUN vuescan 6'), lazy.group[("6")].toscreen(), desc="Scanner")
        ]),
    # dunst ---------------------------------------------------------#
    KeyChord([mod], "m", [
        Key([mod], "t", lazy.spawn(lbin+'dunst_toggle.sh'), desc="dunst toggle"),
        Key([mod], "c", lazy.spawn('dunstctl close'), desc="close msg dunst"),
        Key([mod], "BackSpace", lazy.spawn('dunstctl close-all'), desc="close all dunst messages"),
        Key([mod], "p", lazy.spawn('dunstctl set-paused toggle'), desc="pause dunst messages"),
        Key([mod], "h", lazy.spawn('dunstctl history-pop'), desc="dunst history")
        ]),
    KeyChord([mod], "v", [
        Key([mod], "i", lazy.group[("a")].toscreen(), lazy.spawn('inga-vstart'), desc="start ingas v Box"),
        Key([mod], "v", lazy.group[("0")].toscreen(), lazy.spawn('virt-manager'), desc="start virtual-manager"),
        ]),
    ]
# /KeyChords

# Group are named, matches kan be added, layout for start can be added
groups = [
        Group("1", layout="monadtall"),
        Group("2", matches=[Match(wm_class=["Navigator","qutebrowser","brave-browser","chromium","Mail"])], layout="treetab"),
        Group("3", layout="monadtall"),
        Group("4", layout="monadtall"),
        Group("5", matches=[Match(wm_class=["libreoffice","gimp"])], layout="monadtall"),
        Group("6", matches=[Match(wm_class=["darktable"])], layout="monadwide"),
        Group("7", layout="monadwide"),
        Group("8", matches=[Match(wm_class=["VirtualBox Manager"])], layout="bsp"),
        Group("9", matches=[Match(title=["RANGER"])], layout="bsp"),
        Group("0", matches=[Match(wm_class=["virt-manager"])], layout="bsp"),
        Group("a", layout="bsp"),
        # Groups to consider: a b e l m s u w x y z (æ ø å)
        ]
#
#
# !groups
# ------------------------------------------------------------------ #

# ------------------------------------------------------------------ #
# moving around - move to group (mod+1 etc)
for i in groups:
    keys.extend([
        Key([mod], i.name, lazy.group[i.name].toscreen(), desc="Switch to group {}".format(i.name)),
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name, switch_group=True), desc="Switch to & move focused window to group {}".format(i.name)),
    ])
#
#
# !moving around
# ------------------------------------------------------------------ #

layout_theme = {"border_width": 2,
                "margin": 8,
                "border_focus": "#99aaaa",
                "border_normal": "#4c5601"
                }
# layout_theme = init_layout_theme()


layouts = [
    layout.Bsp(**layout_theme),
    layout.MonadTall(**layout_theme),
    layout.MonadWide(**layout_theme),
    # layout.Max(),
    # layout.Floating(),
    layout.TreeTab(
         font = "Ubuntu",
         fontsize = 10,
         sections = ["WEB"],
         section_fontsize = 10,
         inactive_bg = "a9a1e1",
         inactive_fg = "1c1f24",
         panel_width = 200
         ),
]
#
#
# layouts


# ------------------------------------------------------------------ #
# Append scratchpad with dropdowns to groups
groups.append(ScratchPad('scratchpad', [
    # DropDown('term', '/usr/local/bin/st', width=0.8, height=0.5, x=0.1, y=0.1, opacity=1),
    DropDown('mixer', 'alacritty -e alsamixer', width=0.8, height=0.6, x=0.3, y=0.1, opacity=1),
    DropDown('speed', '/usr/bin/speedcrunch', x=0.4, y=0.3, width=0.4, opacity=1, on_focus_lost_hide=False),
    DropDown('bitw', 'bitwarden-desktop', width=0.6, height=0.6, x=0.3, y=0.1, opacity=1, on_focus_hide=False),
]))

# ------------------------------------------------------------------ #
# widgets
widget_defaults = dict(
        font='sans',
        fontsize=12,
        padding=5,
        )
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        bottom=bar.Bar(
            [
                widget.GroupBox(),
                widget.Sep(),
                widget.CurrentLayoutIcon(custom_icon_paths = [os.path.expanduser("~/.config/qtile/icons")], padding = 0, scale = 0.6),
                widget.CurrentLayout(),
                widget.Sep(),
                widget.Spacer(),
                widget.Chord(),
                widget.Prompt(),
                widget.Sep(),
                widget.DF(foreground = '#888888', measure = 'G', warn_space = 15, visible_on_warn = False, partition = '/', format = '  /  {r:.0f}% '),      ## warn if / is getting full
                widget.DF(foreground = '#888888', measure = 'G', warn_space = 30, visible_on_warn = False, partition = '/home', format = '|  ~  {r:.0f}% '),      ## warn if / is getting full
                widget.DF(foreground = '#888888', measure = 'G', warn_space = 60, visible_on_warn = False, partition = '/home/jan/.local/share/virt', format = '|  virt  {r:.0f}% '),      ## warn if / is getting full
                widget.DF(foreground = '#888888', measure = 'G', warn_space = 60, visible_on_warn = False, partition = '/home/jan/.ulocal/data', format = '|  data  {r:.0f}% '),      ## warn if / is getting full
                widget.Sep(),
                widget.ThermalZone(fgcolor_crit = '#ff0000'),
                widget.Sep(),
                widget.Memory(measure_mem = 'G', mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(terminal + ' -e glances')}),
                widget.CPUGraph(frequency = 1,border_color = '#888888',graph_color = '#888888',fill_color = '#888888'),
                widget.Spacer(),
                widget.Volume(fmt = 'Vol: {}'),
                widget.KeyboardLayout(configured_keyboards = ['no','us']),
                widget.CapsNumLockIndicator(),
                widget.Sep(),
                widget.NetGraph(type = 'line',line_width = 1),
                widget.Sep(),
                widget.Clock(format='%Y-%m-%d %a %H:%M '),
                widget.Sep(),
                widget.Systray(),
            ],
            24,
        ),
    ),
]
#
#
# !widgets
# ------------------------------------------------------------------ #



# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False




# ------------------------------------------------------------------ #
# Floating rules
floating_layout = layout.Floating(float_rules=[
    *layout.Floating.default_float_rules,
    Match(wm_class='speedcrunch'),
    Match(wm_class='bitwarden'),
])
#
#
# !Floating rules
# ------------------------------------------------------------------ #


auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.


wmname = "LG3D"

