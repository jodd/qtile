#!/bin/bash

# force ranger to read ~/.config/ranger/rc.conf and not load it twice
# export RANGER_LOAD_DEFAULT_RC=false

# dex --autostart --environment qtile &
/usr/bin/numlockx on &
dunst &
nitrogen --restore --set-zoom-fill &
picom &



